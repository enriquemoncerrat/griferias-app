/**
 * View Models used by Spring MVC REST controllers.
 */
package ar.com.griferias.app.web.rest.vm;

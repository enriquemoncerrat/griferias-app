package ar.com.griferias.app;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("ar.com.griferias.app");

        noClasses()
            .that()
                .resideInAnyPackage("ar.com.griferias.app.service..")
            .or()
                .resideInAnyPackage("ar.com.griferias.app.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..ar.com.griferias.app.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
